﻿using Restriccion2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonimoTipo3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Producto> productos = Producto.getProductos();

            var productosInfos =
                from p in productos
                select new
                {
                    p.ProductName,
                    p.Category,
                    Precio = p.UnitPrice
                };
            Console.WriteLine("Info de  Productos");
            foreach (var productoInfo in productosInfos)
            {
                Console.WriteLine("{0} es en la categoria {1}  y con un costo {2} por unidad",
                    productoInfo.ProductName, productoInfo.Category, productoInfo.Precio);
            }
            Console.ReadKey();
        }
    }
}
