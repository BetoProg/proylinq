﻿using RepositorioData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelectMany2
{
    class Many2
    {
        static void Main(string[] args)
        {
            List<Cliente> clientes = Cliente.getClientes();

            var ordenes = from c in clientes
                          from o in c.Orders
                          where o.Total < 500.00M
                          select new { c.CustomerID, o.OrderID, o.Total };
            foreach (var o in ordenes) {
                Console.WriteLine(o);
            }
            Console.ReadKey();
        }
    }
}
