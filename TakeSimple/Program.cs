﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeSimple
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numeros = { 5, 4, 3, 10, 8, 3, 2, 6, 1, 0 };

            var primer3Numeros = numeros.Take(3);
            Console.WriteLine("Los primeros tres numeros: ");

            foreach(var n in primer3Numeros){
                Console.WriteLine(n);
            }
            Console.ReadKey();
        }
    }
}
