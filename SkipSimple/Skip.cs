﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkipSimple
{
    class Skip
    {
        static void Main(string[] args)
        {
            int[] numeros = { 4, 5, 7, 1, 2, 6, 8, 3, 9,0 };

            var todosNumerosPrimer4 = numeros.Skip(4);

            Console.WriteLine("Todos pero los cuatro primeros numeros");

            foreach(var n in todosNumerosPrimer4){
                Console.WriteLine(n);
            }
            Console.ReadKey();
        }
    }
}
