﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupBy2
{
    class Agrupacion
    {
        static void Main(string[] args)
        {
            string[] palabras = { "fresas", "duraznos", "naranjas", "uvas", "peras", "tigres", "chango", "perro" };
            var grupoPalabras = (from w in palabras
                                group w by w[0] into g
                                select new { PrimeraLetra = g.Key, Palabras = g })
                                .OrderBy(w=>w.PrimeraLetra);

            foreach (var g in grupoPalabras)
            {
                Console.WriteLine("Palabras que empiezan con la letra {0}:", g.PrimeraLetra);
                foreach (var w in g.Palabras) {
                    Console.WriteLine(w);
                }
            }
            Console.ReadKey();
        }
    }
}
