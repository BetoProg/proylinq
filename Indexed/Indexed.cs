﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexed
{
    class Indexed
    {
        static void Main(string[] args)
        {
            int[] numeros = { 5, 7, 6, 9, 0, 1, 2, 3, 8, 4 };

            var numsInPlace = numeros.Select((num, index) => new { Num = num, InPlace = (num == index) });
            Console.WriteLine("Numero: en su lugar?");
            foreach (var n in numsInPlace)
            {
                Console.WriteLine("{0} {1}",n.Num,n.InPlace);
            }
            Console.ReadKey();
        }
    }
}
