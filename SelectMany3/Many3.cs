﻿using RepositorioData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelectMany3
{
    class Many3
    {
        static void Main(string[] args)
        {
            List<Cliente> cliente = Cliente.getClientes();

            var ordenes = from c in cliente
                          from o in c.Orders
                          where o.OrderDate >= new DateTime(1998, 1, 1)
                          select new { c.CustomerID, o.OrderID, o.OrderDate };

            foreach (var order in ordenes.OrderBy(o=>o.OrderDate)) {
                Console.WriteLine(order);
            }
            Console.ReadKey();
        }
    }
}
