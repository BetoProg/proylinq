﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelectMany1
{
    class ManyUno
    {
        static void Main(string[] args)
        {
            int[] numberosA = { 0, 2, 4, 5, 6, 8, 9 };
            int[] numberosB = { 1, 3, 5, 7, 8 };

            var pares = from a in numberosA
                        from b in numberosB
                        where a < b
                        select new { a, b };

            Console.WriteLine("Pares donde a < b:");
            foreach (var par in pares)
            {
                Console.WriteLine("{0} en menos de {1}",par.a,par.b);
            }
            Console.ReadKey();
        }
    }
}
