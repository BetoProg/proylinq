﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositorioData;
namespace SelectManyIndexed
{
    class Indexed
    {
        static void Main(string[] args)
        {
            var clientes = Cliente.getClientes();
            var clienteOrdenes = clientes.SelectMany(
                (cli, cliIndex) => cli.Orders.Select(o => "Cliente #" + (cliIndex + 1) +
                    " tiene una orden con IDOrden " + o.OrderID));

            foreach(var cli in clienteOrdenes){
                Console.WriteLine(cli);
            }
            Console.ReadKey();
        }
    }
}
