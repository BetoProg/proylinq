﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositorioData;
namespace SkipNested
{
    class SkipNested
    {
        static void Main(string[] args)
        {
            var waOrdenes = from c in Cliente.getClientes()
                            from o in c.Orders
                            where c.Region == "WA"
                            select new { c.CustomerID, o.OrderID, o.OrderDate };

            var allOrders = waOrdenes.Skip(2);

            Console.WriteLine("Todas exepto las dos primeras de wa:");
            foreach (var order in allOrders) {
                Console.WriteLine(order);
            }
            Console.ReadKey();
        }
    }
}
