﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositorioData;
namespace SelectManyMultiple
{
    class Multiple
    {
        static void Main(string[] args)
        {
            var clientes = Cliente.getClientes();
            var fechaCorta = new DateTime(1997,1,1);

            var ordenes = (from c in clientes
                           where c.Region == "WA"
                           from o in c.Orders
                           where o.OrderDate >= fechaCorta
                           select new { c.CustomerID, o.OrderDate }).ToArray();

            for (int i=0;i<=ordenes.Length-1;i++) {
                Console.WriteLine(ordenes[i]);
            }
            Console.ReadKey();
        }
    }
}
