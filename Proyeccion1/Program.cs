﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyeccion1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numeros = { 5, 8, 3, 1, 7, 4, 9, 2, 0 };
            var numeroSumadoUno = from n in numeros
                                  select n + 1;
            Console.WriteLine("Los numeros sumados son:");
            var i = 0;
            while (i <= numeroSumadoUno.Count()-1) {
                Console.WriteLine(numeroSumadoUno.ToArray()[i]);
                i++;
            }
            Console.ReadKey();
        }
    }
}
