﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restricciones1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numeros={5,4,1,3,9,8,6,7,2,0};
            var lowNums = from n in numeros where n < 5 select n;

            Console.WriteLine("Numeros < 5: ");
            foreach(var item in lowNums){
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    }
}
