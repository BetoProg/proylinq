﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Restriccion2;
namespace Restriccion2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Producto> productos = Producto.getProductos();

            var sinExitencias = from p in productos
                                  where p.UnitsInStock == 0
                                  select p;
            Console.WriteLine("productos sin existencias son: ");
            foreach (var item in sinExitencias) {
                Console.WriteLine("{0} no tiene existencias!", item.ProductName);
            }
            Console.ReadKey();
        }
    }
}
