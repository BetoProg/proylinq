﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositorioData;
namespace TakeNested
{
    class TakeNested
    {
        static void Main(string[] args)
        {
            var clientes = Cliente.getClientes();
            var or = 0;
            var primeros3WaOrdenes = (
                    from c in clientes
                    from o in c.Orders
                    where c.Region == "WA"
                    select new { c.CustomerID, o.OrderID, o.OrderDate })
                    .Take(3).ToArray();

            Console.WriteLine("Los primeras 3 ordenes en WA: ");

            while (or <= primeros3WaOrdenes.Length-1) {
                Console.WriteLine(primeros3WaOrdenes[or]);
                or++;
            }
            Console.ReadKey();
        }
    }
}
