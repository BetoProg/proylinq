﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupBy1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numeros = { 5, 6, 9, 4, 2, 3, 1, 7,0,5};
            var numeroGrupos = from n in numeros
                               group n by n % 5 into g
                               select new { recordaotio = g.Key, Numeros = g };

            foreach(var g in numeroGrupos){
                Console.WriteLine("Numeros con un recordatorio de {0} donde estan divididos entre 5", g.recordaotio);
                foreach (var n in g.Numeros) {
                    Console.WriteLine(n);
                }
            }
            Console.ReadKey();
        }
    }
}
