﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonimoTipo1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] palabras = { "mANZANA", "fReSa", "cHErrY", "dUrAzNo" };

            var mayusMinusPalabras = from w in palabras
                                     select new { Upper = w.ToUpper(), Lower = w.ToLower() };
            foreach(var ul in mayusMinusPalabras){
                Console.WriteLine("En Mayusculas {0}, En Minusculas {1}", ul.Upper, ul.Lower);
            }
            Console.ReadKey();
        }
    }
}
