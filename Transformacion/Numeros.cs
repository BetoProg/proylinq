﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformacion
{
    class Numeros
    {
        static void Main(string[] args)
        {
            int[] numeros = { 8, 5, 9, 3, 1, 7, 2, 0, 6,4};
            string[] cadenas = { "cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve"};
            var textNumeros = from n in numeros select cadenas[n];

            Console.WriteLine("Cadena de numeros:");
            foreach (var s in textNumeros) {
                Console.WriteLine(s);
            }
            Console.ReadKey();
        }
    }
}
