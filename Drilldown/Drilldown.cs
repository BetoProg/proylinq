﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositorioData;
namespace Drilldown
{
    class Drilldown
    {
        static void Main(string[] args)
        {
            List<Cliente> clientes = Cliente.getClientes();

            var waClientes = from c in clientes
                             where c.Region == "wa"
                             select c;
            Console.WriteLine("Clientes que tienen ordenes de Washigton");
            foreach (var cliente in waClientes) {
                Console.WriteLine("Clientes {0} {1}", 
                    cliente.CustomerID, cliente.CompanyName);

                foreach(var order in cliente.Orders){
                    Console.WriteLine("Ordenes: {0} {1}", order.OrderID, order.OrderDate);
                }
            }
            Console.ReadKey();
        }
    }
}
