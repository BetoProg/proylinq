﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace toArray
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] doubles = { 1.7, 2.3, 1.9, 4.1, 2.9 };
            var ordenarDoubles = from dou in doubles
                                 orderby dou descending
                                 select dou;
            var doubleArray = ordenarDoubles.ToArray();
            Console.WriteLine("Numeros ordenados: ");
            int d = 0;
            while (d<=doubleArray.Length-1)
            {
                Console.WriteLine(doubleArray[d]);
                d++;
            }
            Console.ReadKey();
        }
    }
}
