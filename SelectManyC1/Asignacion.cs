﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositorioData;
namespace SelectManyC1
{
    class Asignacion
    {
        static void Main(string[] args)
        {
            var clientes = Cliente.getClientes();

            var ordenes = from c in clientes
                        from o in c.Orders
                        where o.Total >= 2000.0M
                        select new { c.CustomerID, o.OrderID, o.Total };
            int x = 0;

            while(x<= ordenes.Count()-1)
            {
                Console.WriteLine(ordenes.ToArray()[x]);
                x++;
            }

            Console.ReadKey();
        }
    }
}
