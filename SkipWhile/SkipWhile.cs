﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkipWhile
{
    class SkipWhile
    {
        static void Main(string[] args)
        {
            int[] numeros = { 5,4,8,3,7,2,0,1,9,6};

            var allButFirst3Numeros = numeros.SkipWhile(n => n % 3 != 0);

            Console.WriteLine("Todos los elementos a partir del primer elemento divisible por 3:");

            foreach (var n in allButFirst3Numeros) {
                Console.WriteLine(n);
            }
            Console.ReadKey();
        }
    }
}
