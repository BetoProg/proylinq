﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculador
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void onBtn(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            tb.Text += b.Content.ToString();
        }

        private void onResult(object sender, RoutedEventArgs e) {
            try
            {
                Resultado();
            }
            catch (Exception ex)
            {

                tb.Text = "Ha ocurrido un error: " + ex.Message;
            }
        }

        private void onCerrar(object sender, RoutedEventArgs e) {
            Application.Current.Shutdown();
        }

        private void Resultado() {
            string op;
            int iOp = 0;
            if (tb.Text.Contains("+"))
            {
                iOp = tb.Text.IndexOf("+");
            }
            else if(tb.Text.Contains("-")){
                iOp = tb.Text.IndexOf("-");
            }
            else if (tb.Text.Contains("*")) {
                iOp = tb.Text.IndexOf("*");
            }
            else if (tb.Text.Contains("/"))
            {
                iOp = tb.Text.IndexOf("/");
            }
            else { 
            
            }

            op = tb.Text.Substring(iOp, 1);
            double op1 = double.Parse(tb.Text.Substring(0,iOp));
            double op2 = double.Parse(tb.Text.Substring(iOp + 1, tb.Text.Length - iOp - 1));

            if (op == "+")
            {
                tb.Text += "=" + (op1 + op2);
            }
            else if (op == "-")
            {
                tb.Text += "=" + (op1 - op2);
            }
            else if (op == "*")
            {
                tb.Text += "=" + (op1 * op2);
            }
            else
            {
                tb.Text += "=" + (op1 / op2);
            }
        }
    }
}
