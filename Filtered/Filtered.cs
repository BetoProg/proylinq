﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filtered
{
    class Filtered
    {
        static void Main(string[] args)
        {
            int[] numeros = { 6, 7, 8, 9, 3, 4, 0, 1, 5 };

            string[] digitos = { "cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve" };

            var lowNumeros = from n in numeros
                where n < 5 select digitos[n];

            Console.WriteLine("Numeros menores de 5:");

            foreach(var num in lowNumeros)
            {
                Console.WriteLine(num);
            }
            Console.ReadKey();
        }
    }
}
