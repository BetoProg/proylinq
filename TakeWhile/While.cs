﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeWhile
{
    class While
    {
        static void Main(string[] args)
        {
            int[] numberos = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            var query = numberos.TakeWhile(n => n < 6);

            Console.WriteLine("Primeros 6:");
            foreach(var m in query){
                Console.WriteLine(m);
            }
            Console.ReadKey();
        }
    }
}
