﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonimoTipo2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numeros = {5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };
            string[] strings = { "cero", "uno", "dos", "tres", "cuatro", "cinco", "sieis", "siete", "ocho", "nueve" };

            var digitOddEvens =
            from n in numeros
            select new { Digit = strings[n], Even = (n % 2 == 0) };

            foreach (var d in digitOddEvens)
            {
                Console.WriteLine("El numero {0} es {1}.", d.Digit, d.Even ? "par" : "impar");
            }
            Console.ReadKey();
        }
    }
}
