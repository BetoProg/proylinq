﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResIndexed
{
    class Indexed
    {
        static void Main(string[] args)
        {
            string[] digitos = { "cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve" };
            var digitosCortos = digitos.Where((digito, index) => digito.Length < index);
            Console.WriteLine("Digitos Cortos:");
            foreach (var d in digitosCortos) {
                Console.WriteLine("La palabra {0} es mas corta que su valor", d);
            }
            Console.ReadKey();
        }
    }
}
