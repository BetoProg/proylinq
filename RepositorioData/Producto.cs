﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restriccion2
{
    public class Producto
    {
       public int ProductID { get; set; }
       public string ProductName { get; set; }
       public string Category { get; set; }
       public decimal UnitPrice { get; set; }
       public int UnitsInStock { get; set; }

       public static List<Producto> getProductos(){
           var lista = new List<Producto> { 
               new Producto {ProductID=1, ProductName="Telefonos",Category="Electronicos",UnitPrice=485.25M,UnitsInStock=12 },
               new Producto {ProductID=2, ProductName="Mesa",Category="Muebles",UnitPrice=1008.20M,UnitsInStock=10 },
               new Producto {ProductID=3, ProductName="Ventilador",Category="Electrodomestico",UnitPrice=270.5M,UnitsInStock=0 },
               new Producto {ProductID=4, ProductName="Computadora",Category="Electronicos",UnitPrice=7580.50M,UnitsInStock=25 },
               new Producto {ProductID=5, ProductName="Libreta",Category="Papeleria",UnitPrice=19.8M,UnitsInStock=55 },
               new Producto {ProductID=6, ProductName="Juego de Desarmadores",Category="Herramientas",UnitPrice=320.5M,UnitsInStock=7 },
               new Producto {ProductID=7, ProductName="Silla",Category="Muebles",UnitPrice=820.35M,UnitsInStock=15 },
               new Producto {ProductID=8, ProductName="Filtro de Ventilador",Category="Electronicos",UnitPrice=147.50M,UnitsInStock=0 },
               new Producto {ProductID=9, ProductName="Esmeril de Hierro",Category="Herramientas",UnitPrice=345.7M,UnitsInStock=18 },
               new Producto {ProductID=10, ProductName="Mouse Inalambrico",Category="Electronicos",UnitPrice=150.8M,UnitsInStock=78 }
           };
           return lista;
       }
    }
}
