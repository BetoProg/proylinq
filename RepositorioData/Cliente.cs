﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RepositorioData
{
    public class Cliente
    {
        public string CustomerID { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public Ordenes[] Orders { get; set; }

        public static List<Cliente> getClientes() {
            var listaCliente = (from c in XDocument.Load("Customers.xml").
                                    Root.Elements("customer")
                                select new Cliente
                                {
                                    CustomerID = (string)c.Element("id"),
                                    CompanyName = (string)c.Element("name"),
                                    Address = (string)c.Element("address"),
                                    City = (string)c.Element("city"),
                                    Region = (string)c.Element("region"),
                                    PostalCode = (string)c.Element("postalcode"),
                                    Country = (string)c.Element("country"),
                                    Phone = (string)c.Element("phone"),
                                    Fax = (string)c.Element("fax"),
                                    Orders = (
                                    from o in c.Elements("orders").Elements("order") 
                                    select new Ordenes { 
                                        OrderID = (int)o.Element("id"),
                                        OrderDate = (DateTime)o.Element("orderdate"),
                                        Total = (decimal)o.Element("total")
                                    }).ToArray()
                                }).ToList();
            return listaCliente;
        }
    }

   
}
