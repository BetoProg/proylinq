﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeWhileIndexed
{
    class Indexed
    {
        static void Main(string[] args)
        {
            int[] numeros = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            var firstSmallNumeros = numeros.TakeWhile((n, index) => n >= index);

            Console.WriteLine("Primeros números no menos que su posición");

            foreach (var n in firstSmallNumeros) {
                Console.WriteLine(n);
            };
            Console.ReadKey();
        }
    }
}
